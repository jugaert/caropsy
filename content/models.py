from django.db import models


class MenuItem(models.Model):
    id = models.CharField(max_length=50, primary_key=True)
    label = models.CharField(max_length=50)
    uri = models.CharField(max_length=255, default='/')
    position = models.IntegerField(default=0)
    parent = models.ForeignKey('self', blank=True, null=True, related_name='children', on_delete=models.CASCADE)

    def __str__(self):
        return self.id


class ContentPage(models.Model):
    page = models.OneToOneField(MenuItem, on_delete = models.CASCADE)
    seo_title = models.CharField(max_length = 50, blank = True)
    seo_h1 = models.CharField(max_length = 50)
    main_content = models.TextField()
    extra_content = models.TextField(blank = True)

    def __str__(self):
        return self.page.id
