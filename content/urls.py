from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="home"),
    path('contact', views.contact, name = "contact"),
    path('<slug:call>', views.index, name="home"),
    path('article/<slug:name>', views.index, name="article")
]
