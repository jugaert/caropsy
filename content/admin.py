from django.contrib import admin
from .models import *

admin.site.register(ContentPage)
admin.site.register(MenuItem)

