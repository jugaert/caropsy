from .models import MenuItem


def menu_render(request):
    menu = MenuItem.objects.all().order_by('position')

    return {'menu': list(menu.values())}
