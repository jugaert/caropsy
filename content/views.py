from django.shortcuts import render
from .models import *


def index(request, call='accueil'):
    content = ContentPage.objects.get(page=call)
    context = {'content': content}
    return render(request, 'content/index.html', context)


def contact(request):
    return render(request, 'content/contact.html', {})
